<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Laravel Documentation</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset("/vendor/scribe/css/theme-default.style.css") }}" media="screen">
    <link rel="stylesheet" href="{{ asset("/vendor/scribe/css/theme-default.print.css") }}" media="print">

    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>

    <link rel="stylesheet"
          href="https://unpkg.com/@highlightjs/cdn-assets@10.7.2/styles/obsidian.min.css">
    <script src="https://unpkg.com/@highlightjs/cdn-assets@10.7.2/highlight.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jets/0.14.1/jets.min.js"></script>

    <style id="language-style">
        /* starts out as display none and is replaced with js later  */
                    body .content .bash-example code { display: none; }
                    body .content .javascript-example code { display: none; }
            </style>

    <script>
        var baseUrl = "http://test.test";
        var useCsrf = Boolean();
        var csrfUrl = "/sanctum/csrf-cookie";
    </script>
    <script src="{{ asset("/vendor/scribe/js/tryitout-3.33.0.js") }}"></script>

    <script src="{{ asset("/vendor/scribe/js/theme-default-3.33.0.js") }}"></script>

</head>

<body data-languages="[&quot;bash&quot;,&quot;javascript&quot;]">

<a href="#" id="nav-button">
    <span>
        MENU
        <img src="{{ asset("/vendor/scribe/images/navbar.png") }}" alt="navbar-image" />
    </span>
</a>
<div class="tocify-wrapper">
    
            <div class="lang-selector">
                                            <button type="button" class="lang-button" data-language-name="bash">bash</button>
                                            <button type="button" class="lang-button" data-language-name="javascript">javascript</button>
                    </div>
    
    <div class="search">
        <input type="text" class="search" id="input-search" placeholder="Search">
    </div>

    <div id="toc">
                                                                            <ul id="tocify-header-0" class="tocify-header">
                    <li class="tocify-item level-1" data-unique="introduction">
                        <a href="#introduction">Introduction</a>
                    </li>
                                            
                                                                    </ul>
                                                <ul id="tocify-header-1" class="tocify-header">
                    <li class="tocify-item level-1" data-unique="authenticating-requests">
                        <a href="#authenticating-requests">Authenticating requests</a>
                    </li>
                                            
                                                </ul>
                    
                    <ul id="tocify-header-2" class="tocify-header">
                <li class="tocify-item level-1" data-unique="endpoints">
                    <a href="#endpoints">Endpoints</a>
                </li>
                                    <ul id="tocify-subheader-endpoints" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="endpoints-GETapi-workers">
                        <a href="#endpoints-GETapi-workers">Display a listing of the resource.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-GETapi-workers--id-">
                        <a href="#endpoints-GETapi-workers--id-">Display the specified resource.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-POSTapi-users-register">
                        <a href="#endpoints-POSTapi-users-register">Register a newly created user.</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-POSTapi-users-login">
                        <a href="#endpoints-POSTapi-users-login">Registered user Login</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-POSTapi-workers-login">
                        <a href="#endpoints-POSTapi-workers-login">Registered user Login</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-POSTapi-workers-logout">
                        <a href="#endpoints-POSTapi-workers-logout">Worker Logout user Login</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-GETapi-workers-request--id-">
                        <a href="#endpoints-GETapi-workers-request--id-">Display the request holiday of individual</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-GETapi-workers-remaining--id-">
                        <a href="#endpoints-GETapi-workers-remaining--id-">Display the remaining holiday of individual</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-POSTapi-workers-newrequest">
                        <a href="#endpoints-POSTapi-workers-newrequest">POST api/workers/newrequest</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-POSTapi-users-logout">
                        <a href="#endpoints-POSTapi-users-logout">User Logout user Login</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-GETapi-users-loggeduser">
                        <a href="#endpoints-GETapi-users-loggeduser">Get User Logged in user with authorization enable</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-POSTapi-users-worker-create">
                        <a href="#endpoints-POSTapi-users-worker-create">Enable to create worker</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-GETapi-users-requests-list">
                        <a href="#endpoints-GETapi-users-requests-list">List of vacation request</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-POSTapi-users-update-request">
                        <a href="#endpoints-POSTapi-users-update-request">Update Vacation Request Status</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-GETapi-users-requests--id-">
                        <a href="#endpoints-GETapi-users-requests--id-">List of vacation request of individual workers</a>
                    </li>
                                    <li class="tocify-item level-2" data-unique="endpoints-GETapi-users-overrequest">
                        <a href="#endpoints-GETapi-users-overrequest">Display the overlapping request</a>
                    </li>
                                                    </ul>
                            </ul>
        
                        
            </div>

            <ul class="toc-footer" id="toc-footer">
                            <li><a href="{{ route("scribe.postman") }}">View Postman collection</a></li>
                            <li><a href="{{ route("scribe.openapi") }}">View OpenAPI spec</a></li>
                            <li><a href="http://github.com/knuckleswtf/scribe">Documentation powered by Scribe ✍</a></li>
                    </ul>
        <ul class="toc-footer" id="last-updated">
        <li>Last updated: June 29 2022</li>
    </ul>
</div>

<div class="page-wrapper">
    <div class="dark-box"></div>
    <div class="content">
        <h1 id="introduction">Introduction</h1>
<p>This documentation aims to provide all the information you need to work with our API.</p>
<aside>As you scroll, you'll see code examples for working with the API in different programming languages in the dark area to the right (or as part of the content on mobile).
You can switch the language used with the tabs at the top right (or from the nav menu at the top left on mobile).</aside>
<blockquote>
<p>Base URL</p>
</blockquote>
<pre><code class="language-yaml">http://test.test</code></pre>

        <h1 id="authenticating-requests">Authenticating requests</h1>
<p>This API is not authenticated.</p>

        <h1 id="endpoints">Endpoints</h1>

    

            <h2 id="endpoints-GETapi-workers">Display a listing of the resource.</h2>

<p>
</p>



<span id="example-requests-GETapi-workers">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://test.test/api/workers" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/workers"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-workers">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
x-ratelimit-limit: 60
x-ratelimit-remaining: 59
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;success&quot;: true,
    &quot;message&quot;: &quot;List of Workers&quot;,
    &quot;data&quot;: [
        {
            &quot;id&quot;: 55,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page6@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:09:32&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:09:32.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-29T09:59:38.000000Z&quot;
        },
        {
            &quot;id&quot;: 57,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page8@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:36:54&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:36:54.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:36:54.000000Z&quot;
        },
        {
            &quot;id&quot;: 58,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page9@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:37:27&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:37:27.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:37:27.000000Z&quot;
        },
        {
            &quot;id&quot;: 59,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page10@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:38:06&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:38:06.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:38:06.000000Z&quot;
        },
        {
            &quot;id&quot;: 60,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page11@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:38:44&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:38:44.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:38:44.000000Z&quot;
        },
        {
            &quot;id&quot;: 61,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page12@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:40:44&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:40:44.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:40:44.000000Z&quot;
        },
        {
            &quot;id&quot;: 62,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page13@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:41:58&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:41:58.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:41:58.000000Z&quot;
        },
        {
            &quot;id&quot;: 63,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page14@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:42:51&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:42:51.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:42:51.000000Z&quot;
        },
        {
            &quot;id&quot;: 64,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page15@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:43:51&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:43:51.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:43:51.000000Z&quot;
        },
        {
            &quot;id&quot;: 65,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page16@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:44:15&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:44:15.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:44:15.000000Z&quot;
        },
        {
            &quot;id&quot;: 66,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page17@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:50:10&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:50:10.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:50:10.000000Z&quot;
        },
        {
            &quot;id&quot;: 67,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page18@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:51:40&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:51:40.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:51:40.000000Z&quot;
        },
        {
            &quot;id&quot;: 68,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page19@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:54:57&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:54:57.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:54:57.000000Z&quot;
        },
        {
            &quot;id&quot;: 69,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page20@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:55:57&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:55:57.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:55:57.000000Z&quot;
        },
        {
            &quot;id&quot;: 70,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page21@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:56:43&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:56:43.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:56:43.000000Z&quot;
        },
        {
            &quot;id&quot;: 71,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page22@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 06:57:34&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T06:57:34.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T06:57:34.000000Z&quot;
        },
        {
            &quot;id&quot;: 72,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page23@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 07:04:44&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T07:04:44.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-29T11:54:13.000000Z&quot;
        },
        {
            &quot;id&quot;: 73,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page24@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 07:08:43&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T07:08:43.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T07:08:43.000000Z&quot;
        },
        {
            &quot;id&quot;: 74,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page25@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 07:09:32&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T07:09:32.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T07:09:32.000000Z&quot;
        },
        {
            &quot;id&quot;: 75,
            &quot;name&quot;: &quot;larrry Page&quot;,
            &quot;email&quot;: &quot;page26@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-28 07:42:04&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-28T07:42:04.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-28T07:42:04.000000Z&quot;
        },
        {
            &quot;id&quot;: 77,
            &quot;name&quot;: &quot;Calvin Clinton&quot;,
            &quot;email&quot;: &quot;calvin.clinton@gmail.com&quot;,
            &quot;joined_date&quot;: &quot;2022-06-29 12:27:03&quot;,
            &quot;date_of_birth&quot;: null,
            &quot;contact&quot;: &quot;&quot;,
            &quot;street&quot;: &quot;&quot;,
            &quot;city&quot;: &quot;&quot;,
            &quot;postal_code&quot;: &quot;&quot;,
            &quot;position&quot;: &quot;&quot;,
            &quot;department&quot;: &quot;&quot;,
            &quot;created_at&quot;: &quot;2022-06-29T12:27:03.000000Z&quot;,
            &quot;updated_at&quot;: &quot;2022-06-29T12:27:03.000000Z&quot;
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-workers" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-workers"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-workers"></code></pre>
</span>
<span id="execution-error-GETapi-workers" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-workers"></code></pre>
</span>
<form id="form-GETapi-workers" data-method="GET"
      data-path="api/workers"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-workers', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-workers"
                    onclick="tryItOut('GETapi-workers');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-workers"
                    onclick="cancelTryOut('GETapi-workers');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-workers" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/workers</code></b>
        </p>
                    </form>

            <h2 id="endpoints-GETapi-workers--id-">Display the specified resource.</h2>

<p>
</p>



<span id="example-requests-GETapi-workers--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://test.test/api/workers/6" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/workers/6"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-workers--id-">
            <blockquote>
            <p>Example response (500):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
x-ratelimit-limit: 60
x-ratelimit-remaining: 58
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;Attempt to read property \&quot;worker_id\&quot; on null&quot;,
    &quot;exception&quot;: &quot;ErrorException&quot;,
    &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Http\\Resources\\DelegatesToResource.php&quot;,
    &quot;line&quot;: 139,
    &quot;trace&quot;: [
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Bootstrap\\HandleExceptions.php&quot;,
            &quot;line&quot;: 257,
            &quot;function&quot;: &quot;handleError&quot;,
            &quot;class&quot;: &quot;Illuminate\\Foundation\\Bootstrap\\HandleExceptions&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Http\\Resources\\DelegatesToResource.php&quot;,
            &quot;line&quot;: 139,
            &quot;function&quot;: &quot;Illuminate\\Foundation\\Bootstrap\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Foundation\\Bootstrap\\HandleExceptions&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\app\\Http\\Resources\\WorkerResource.php&quot;,
            &quot;line&quot;: 18,
            &quot;function&quot;: &quot;__get&quot;,
            &quot;class&quot;: &quot;Illuminate\\Http\\Resources\\Json\\JsonResource&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Http\\Resources\\Json\\JsonResource.php&quot;,
            &quot;line&quot;: 95,
            &quot;function&quot;: &quot;toArray&quot;,
            &quot;class&quot;: &quot;App\\Http\\Resources\\WorkerResource&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Http\\Resources\\Json\\JsonResource.php&quot;,
            &quot;line&quot;: 241,
            &quot;function&quot;: &quot;resolve&quot;,
            &quot;class&quot;: &quot;Illuminate\\Http\\Resources\\Json\\JsonResource&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;function&quot;: &quot;jsonSerialize&quot;,
            &quot;class&quot;: &quot;Illuminate\\Http\\Resources\\Json\\JsonResource&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Http\\JsonResponse.php&quot;,
            &quot;line&quot;: 87,
            &quot;function&quot;: &quot;json_encode&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\symfony\\http-foundation\\JsonResponse.php&quot;,
            &quot;line&quot;: 51,
            &quot;function&quot;: &quot;setData&quot;,
            &quot;class&quot;: &quot;Illuminate\\Http\\JsonResponse&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Http\\JsonResponse.php&quot;,
            &quot;line&quot;: 32,
            &quot;function&quot;: &quot;__construct&quot;,
            &quot;class&quot;: &quot;Symfony\\Component\\HttpFoundation\\JsonResponse&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ResponseFactory.php&quot;,
            &quot;line&quot;: 101,
            &quot;function&quot;: &quot;__construct&quot;,
            &quot;class&quot;: &quot;Illuminate\\Http\\JsonResponse&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\app\\Http\\Controllers\\Api\\WorkerController.php&quot;,
            &quot;line&quot;: 64,
            &quot;function&quot;: &quot;json&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\ResponseFactory&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php&quot;,
            &quot;line&quot;: 54,
            &quot;function&quot;: &quot;show&quot;,
            &quot;class&quot;: &quot;App\\Http\\Controllers\\Api\\WorkerController&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php&quot;,
            &quot;line&quot;: 45,
            &quot;function&quot;: &quot;callAction&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\Controller&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php&quot;,
            &quot;line&quot;: 261,
            &quot;function&quot;: &quot;dispatch&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\ControllerDispatcher&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php&quot;,
            &quot;line&quot;: 204,
            &quot;function&quot;: &quot;runController&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\Route&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php&quot;,
            &quot;line&quot;: 725,
            &quot;function&quot;: &quot;run&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\Route&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php&quot;,
            &quot;line&quot;: 141,
            &quot;function&quot;: &quot;Illuminate\\Routing\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\Router&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php&quot;,
            &quot;line&quot;: 50,
            &quot;function&quot;: &quot;Illuminate\\Pipeline\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Pipeline\\Pipeline&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php&quot;,
            &quot;line&quot;: 180,
            &quot;function&quot;: &quot;handle&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\Middleware\\SubstituteBindings&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php&quot;,
            &quot;line&quot;: 126,
            &quot;function&quot;: &quot;Illuminate\\Pipeline\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Pipeline\\Pipeline&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php&quot;,
            &quot;line&quot;: 102,
            &quot;function&quot;: &quot;handleRequest&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\Middleware\\ThrottleRequests&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php&quot;,
            &quot;line&quot;: 54,
            &quot;function&quot;: &quot;handleRequestUsingNamedLimiter&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\Middleware\\ThrottleRequests&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php&quot;,
            &quot;line&quot;: 180,
            &quot;function&quot;: &quot;handle&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\Middleware\\ThrottleRequests&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php&quot;,
            &quot;line&quot;: 116,
            &quot;function&quot;: &quot;Illuminate\\Pipeline\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Pipeline\\Pipeline&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php&quot;,
            &quot;line&quot;: 726,
            &quot;function&quot;: &quot;then&quot;,
            &quot;class&quot;: &quot;Illuminate\\Pipeline\\Pipeline&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php&quot;,
            &quot;line&quot;: 703,
            &quot;function&quot;: &quot;runRouteWithinStack&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\Router&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php&quot;,
            &quot;line&quot;: 667,
            &quot;function&quot;: &quot;runRoute&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\Router&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php&quot;,
            &quot;line&quot;: 656,
            &quot;function&quot;: &quot;dispatchToRoute&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\Router&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php&quot;,
            &quot;line&quot;: 167,
            &quot;function&quot;: &quot;dispatch&quot;,
            &quot;class&quot;: &quot;Illuminate\\Routing\\Router&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php&quot;,
            &quot;line&quot;: 141,
            &quot;function&quot;: &quot;Illuminate\\Foundation\\Http\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Foundation\\Http\\Kernel&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php&quot;,
            &quot;line&quot;: 21,
            &quot;function&quot;: &quot;Illuminate\\Pipeline\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Pipeline\\Pipeline&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ConvertEmptyStringsToNull.php&quot;,
            &quot;line&quot;: 31,
            &quot;function&quot;: &quot;handle&quot;,
            &quot;class&quot;: &quot;Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php&quot;,
            &quot;line&quot;: 180,
            &quot;function&quot;: &quot;handle&quot;,
            &quot;class&quot;: &quot;Illuminate\\Foundation\\Http\\Middleware\\ConvertEmptyStringsToNull&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php&quot;,
            &quot;line&quot;: 21,
            &quot;function&quot;: &quot;Illuminate\\Pipeline\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Pipeline\\Pipeline&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TrimStrings.php&quot;,
            &quot;line&quot;: 40,
            &quot;function&quot;: &quot;handle&quot;,
            &quot;class&quot;: &quot;Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php&quot;,
            &quot;line&quot;: 180,
            &quot;function&quot;: &quot;handle&quot;,
            &quot;class&quot;: &quot;Illuminate\\Foundation\\Http\\Middleware\\TrimStrings&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php&quot;,
            &quot;line&quot;: 27,
            &quot;function&quot;: &quot;Illuminate\\Pipeline\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Pipeline\\Pipeline&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php&quot;,
            &quot;line&quot;: 180,
            &quot;function&quot;: &quot;handle&quot;,
            &quot;class&quot;: &quot;Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance.php&quot;,
            &quot;line&quot;: 86,
            &quot;function&quot;: &quot;Illuminate\\Pipeline\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Pipeline\\Pipeline&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php&quot;,
            &quot;line&quot;: 180,
            &quot;function&quot;: &quot;handle&quot;,
            &quot;class&quot;: &quot;Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Http\\Middleware\\HandleCors.php&quot;,
            &quot;line&quot;: 62,
            &quot;function&quot;: &quot;Illuminate\\Pipeline\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Pipeline\\Pipeline&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php&quot;,
            &quot;line&quot;: 180,
            &quot;function&quot;: &quot;handle&quot;,
            &quot;class&quot;: &quot;Illuminate\\Http\\Middleware\\HandleCors&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Http\\Middleware\\TrustProxies.php&quot;,
            &quot;line&quot;: 39,
            &quot;function&quot;: &quot;Illuminate\\Pipeline\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Pipeline\\Pipeline&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php&quot;,
            &quot;line&quot;: 180,
            &quot;function&quot;: &quot;handle&quot;,
            &quot;class&quot;: &quot;Illuminate\\Http\\Middleware\\TrustProxies&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php&quot;,
            &quot;line&quot;: 116,
            &quot;function&quot;: &quot;Illuminate\\Pipeline\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Pipeline\\Pipeline&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php&quot;,
            &quot;line&quot;: 142,
            &quot;function&quot;: &quot;then&quot;,
            &quot;class&quot;: &quot;Illuminate\\Pipeline\\Pipeline&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php&quot;,
            &quot;line&quot;: 111,
            &quot;function&quot;: &quot;sendRequestThroughRouter&quot;,
            &quot;class&quot;: &quot;Illuminate\\Foundation\\Http\\Kernel&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php&quot;,
            &quot;line&quot;: 299,
            &quot;function&quot;: &quot;handle&quot;,
            &quot;class&quot;: &quot;Illuminate\\Foundation\\Http\\Kernel&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php&quot;,
            &quot;line&quot;: 287,
            &quot;function&quot;: &quot;callLaravelOrLumenRoute&quot;,
            &quot;class&quot;: &quot;Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php&quot;,
            &quot;line&quot;: 89,
            &quot;function&quot;: &quot;makeApiCall&quot;,
            &quot;class&quot;: &quot;Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php&quot;,
            &quot;line&quot;: 45,
            &quot;function&quot;: &quot;makeResponseCall&quot;,
            &quot;class&quot;: &quot;Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php&quot;,
            &quot;line&quot;: 35,
            &quot;function&quot;: &quot;makeResponseCallIfConditionsPass&quot;,
            &quot;class&quot;: &quot;Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Extractor.php&quot;,
            &quot;line&quot;: 222,
            &quot;function&quot;: &quot;__invoke&quot;,
            &quot;class&quot;: &quot;Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Extractor.php&quot;,
            &quot;line&quot;: 179,
            &quot;function&quot;: &quot;iterateThroughStrategies&quot;,
            &quot;class&quot;: &quot;Knuckles\\Scribe\\Extracting\\Extractor&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Extractor.php&quot;,
            &quot;line&quot;: 116,
            &quot;function&quot;: &quot;fetchResponses&quot;,
            &quot;class&quot;: &quot;Knuckles\\Scribe\\Extracting\\Extractor&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\knuckleswtf\\scribe\\src\\GroupedEndpoints\\GroupedEndpointsFromApp.php&quot;,
            &quot;line&quot;: 123,
            &quot;function&quot;: &quot;processRoute&quot;,
            &quot;class&quot;: &quot;Knuckles\\Scribe\\Extracting\\Extractor&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\knuckleswtf\\scribe\\src\\GroupedEndpoints\\GroupedEndpointsFromApp.php&quot;,
            &quot;line&quot;: 80,
            &quot;function&quot;: &quot;extractEndpointsInfoFromLaravelApp&quot;,
            &quot;class&quot;: &quot;Knuckles\\Scribe\\GroupedEndpoints\\GroupedEndpointsFromApp&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\knuckleswtf\\scribe\\src\\GroupedEndpoints\\GroupedEndpointsFromApp.php&quot;,
            &quot;line&quot;: 56,
            &quot;function&quot;: &quot;extractEndpointsInfoAndWriteToDisk&quot;,
            &quot;class&quot;: &quot;Knuckles\\Scribe\\GroupedEndpoints\\GroupedEndpointsFromApp&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php&quot;,
            &quot;line&quot;: 55,
            &quot;function&quot;: &quot;get&quot;,
            &quot;class&quot;: &quot;Knuckles\\Scribe\\GroupedEndpoints\\GroupedEndpointsFromApp&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php&quot;,
            &quot;line&quot;: 36,
            &quot;function&quot;: &quot;handle&quot;,
            &quot;class&quot;: &quot;Knuckles\\Scribe\\Commands\\GenerateDocumentation&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php&quot;,
            &quot;line&quot;: 41,
            &quot;function&quot;: &quot;Illuminate\\Container\\{closure}&quot;,
            &quot;class&quot;: &quot;Illuminate\\Container\\BoundMethod&quot;,
            &quot;type&quot;: &quot;::&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php&quot;,
            &quot;line&quot;: 93,
            &quot;function&quot;: &quot;unwrapIfClosure&quot;,
            &quot;class&quot;: &quot;Illuminate\\Container\\Util&quot;,
            &quot;type&quot;: &quot;::&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php&quot;,
            &quot;line&quot;: 37,
            &quot;function&quot;: &quot;callBoundMethod&quot;,
            &quot;class&quot;: &quot;Illuminate\\Container\\BoundMethod&quot;,
            &quot;type&quot;: &quot;::&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php&quot;,
            &quot;line&quot;: 651,
            &quot;function&quot;: &quot;call&quot;,
            &quot;class&quot;: &quot;Illuminate\\Container\\BoundMethod&quot;,
            &quot;type&quot;: &quot;::&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php&quot;,
            &quot;line&quot;: 136,
            &quot;function&quot;: &quot;call&quot;,
            &quot;class&quot;: &quot;Illuminate\\Container\\Container&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\symfony\\console\\Command\\Command.php&quot;,
            &quot;line&quot;: 308,
            &quot;function&quot;: &quot;execute&quot;,
            &quot;class&quot;: &quot;Illuminate\\Console\\Command&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php&quot;,
            &quot;line&quot;: 121,
            &quot;function&quot;: &quot;run&quot;,
            &quot;class&quot;: &quot;Symfony\\Component\\Console\\Command\\Command&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\symfony\\console\\Application.php&quot;,
            &quot;line&quot;: 998,
            &quot;function&quot;: &quot;run&quot;,
            &quot;class&quot;: &quot;Illuminate\\Console\\Command&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\symfony\\console\\Application.php&quot;,
            &quot;line&quot;: 299,
            &quot;function&quot;: &quot;doRunCommand&quot;,
            &quot;class&quot;: &quot;Symfony\\Component\\Console\\Application&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\symfony\\console\\Application.php&quot;,
            &quot;line&quot;: 171,
            &quot;function&quot;: &quot;doRun&quot;,
            &quot;class&quot;: &quot;Symfony\\Component\\Console\\Application&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php&quot;,
            &quot;line&quot;: 102,
            &quot;function&quot;: &quot;run&quot;,
            &quot;class&quot;: &quot;Symfony\\Component\\Console\\Application&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php&quot;,
            &quot;line&quot;: 129,
            &quot;function&quot;: &quot;run&quot;,
            &quot;class&quot;: &quot;Illuminate\\Console\\Application&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        },
        {
            &quot;file&quot;: &quot;E:\\newProject\\htdocs\\holiday-plans-laravel-rylris\\artisan&quot;,
            &quot;line&quot;: 37,
            &quot;function&quot;: &quot;handle&quot;,
            &quot;class&quot;: &quot;Illuminate\\Foundation\\Console\\Kernel&quot;,
            &quot;type&quot;: &quot;-&gt;&quot;
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-workers--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-workers--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-workers--id-"></code></pre>
</span>
<span id="execution-error-GETapi-workers--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-workers--id-"></code></pre>
</span>
<form id="form-GETapi-workers--id-" data-method="GET"
      data-path="api/workers/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-workers--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-workers--id-"
                    onclick="tryItOut('GETapi-workers--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-workers--id-"
                    onclick="cancelTryOut('GETapi-workers--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-workers--id-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/workers/{id}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
                <input type="number"
               name="id"
               data-endpoint="GETapi-workers--id-"
               value="6"
               data-component="url" hidden>
    <br>
<p>The ID of the worker.</p>
            </p>
                    </form>

            <h2 id="endpoints-POSTapi-users-register">Register a newly created user.</h2>

<p>
</p>



<span id="example-requests-POSTapi-users-register">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://test.test/api/users/register" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"name\": \"id\",
    \"email\": \"rziemann@example.com\",
    \"password\": \"exdlb\",
    \"cpassword\": \"quia\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/users/register"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "id",
    "email": "rziemann@example.com",
    "password": "exdlb",
    "cpassword": "quia"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-users-register">
</span>
<span id="execution-results-POSTapi-users-register" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-users-register"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-users-register"></code></pre>
</span>
<span id="execution-error-POSTapi-users-register" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-users-register"></code></pre>
</span>
<form id="form-POSTapi-users-register" data-method="POST"
      data-path="api/users/register"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-users-register', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-users-register"
                    onclick="tryItOut('POSTapi-users-register');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-users-register"
                    onclick="cancelTryOut('POSTapi-users-register');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-users-register" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/users/register</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="name"
               data-endpoint="POSTapi-users-register"
               value="id"
               data-component="body" hidden>
    <br>

        </p>
                <p>
            <b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="email"
               data-endpoint="POSTapi-users-register"
               value="rziemann@example.com"
               data-component="body" hidden>
    <br>
<p>Must be a valid email address.</p>
        </p>
                <p>
            <b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="password"
               data-endpoint="POSTapi-users-register"
               value="exdlb"
               data-component="body" hidden>
    <br>
<p>Must be at least 5 characters.</p>
        </p>
                <p>
            <b><code>cpassword</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="cpassword"
               data-endpoint="POSTapi-users-register"
               value="quia"
               data-component="body" hidden>
    <br>
<p>The value and <code>password</code> must match.</p>
        </p>
        </form>

            <h2 id="endpoints-POSTapi-users-login">Registered user Login</h2>

<p>
</p>



<span id="example-requests-POSTapi-users-login">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://test.test/api/users/login" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"email\": \"ithompson@example.org\",
    \"password\": \"voluptatem\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/users/login"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "ithompson@example.org",
    "password": "voluptatem"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-users-login">
</span>
<span id="execution-results-POSTapi-users-login" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-users-login"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-users-login"></code></pre>
</span>
<span id="execution-error-POSTapi-users-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-users-login"></code></pre>
</span>
<form id="form-POSTapi-users-login" data-method="POST"
      data-path="api/users/login"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-users-login', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-users-login"
                    onclick="tryItOut('POSTapi-users-login');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-users-login"
                    onclick="cancelTryOut('POSTapi-users-login');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-users-login" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/users/login</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="email"
               data-endpoint="POSTapi-users-login"
               value="ithompson@example.org"
               data-component="body" hidden>
    <br>
<p>Must be a valid email address.</p>
        </p>
                <p>
            <b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="password"
               data-endpoint="POSTapi-users-login"
               value="voluptatem"
               data-component="body" hidden>
    <br>

        </p>
        </form>

            <h2 id="endpoints-POSTapi-workers-login">Registered user Login</h2>

<p>
</p>



<span id="example-requests-POSTapi-workers-login">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://test.test/api/workers/login" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"email\": \"torrance77@example.com\",
    \"password\": \"illo\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/workers/login"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "torrance77@example.com",
    "password": "illo"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-workers-login">
</span>
<span id="execution-results-POSTapi-workers-login" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-workers-login"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-workers-login"></code></pre>
</span>
<span id="execution-error-POSTapi-workers-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-workers-login"></code></pre>
</span>
<form id="form-POSTapi-workers-login" data-method="POST"
      data-path="api/workers/login"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-workers-login', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-workers-login"
                    onclick="tryItOut('POSTapi-workers-login');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-workers-login"
                    onclick="cancelTryOut('POSTapi-workers-login');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-workers-login" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/workers/login</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="email"
               data-endpoint="POSTapi-workers-login"
               value="torrance77@example.com"
               data-component="body" hidden>
    <br>
<p>Must be a valid email address.</p>
        </p>
                <p>
            <b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="password"
               data-endpoint="POSTapi-workers-login"
               value="illo"
               data-component="body" hidden>
    <br>

        </p>
        </form>

            <h2 id="endpoints-POSTapi-workers-logout">Worker Logout user Login</h2>

<p>
</p>



<span id="example-requests-POSTapi-workers-logout">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://test.test/api/workers/logout" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/workers/logout"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-workers-logout">
</span>
<span id="execution-results-POSTapi-workers-logout" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-workers-logout"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-workers-logout"></code></pre>
</span>
<span id="execution-error-POSTapi-workers-logout" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-workers-logout"></code></pre>
</span>
<form id="form-POSTapi-workers-logout" data-method="POST"
      data-path="api/workers/logout"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-workers-logout', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-workers-logout"
                    onclick="tryItOut('POSTapi-workers-logout');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-workers-logout"
                    onclick="cancelTryOut('POSTapi-workers-logout');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-workers-logout" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/workers/logout</code></b>
        </p>
                    </form>

            <h2 id="endpoints-GETapi-workers-request--id-">Display the request holiday of individual</h2>

<p>
</p>



<span id="example-requests-GETapi-workers-request--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://test.test/api/workers/request/aut" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/workers/request/aut"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-workers-request--id-">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;Unauthenticated.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-workers-request--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-workers-request--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-workers-request--id-"></code></pre>
</span>
<span id="execution-error-GETapi-workers-request--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-workers-request--id-"></code></pre>
</span>
<form id="form-GETapi-workers-request--id-" data-method="GET"
      data-path="api/workers/request/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-workers-request--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-workers-request--id-"
                    onclick="tryItOut('GETapi-workers-request--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-workers-request--id-"
                    onclick="cancelTryOut('GETapi-workers-request--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-workers-request--id-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/workers/request/{id}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="id"
               data-endpoint="GETapi-workers-request--id-"
               value="aut"
               data-component="url" hidden>
    <br>
<p>The ID of the request.</p>
            </p>
                    </form>

            <h2 id="endpoints-GETapi-workers-remaining--id-">Display the remaining holiday of individual</h2>

<p>
</p>



<span id="example-requests-GETapi-workers-remaining--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://test.test/api/workers/remaining/aut" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/workers/remaining/aut"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-workers-remaining--id-">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;Unauthenticated.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-workers-remaining--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-workers-remaining--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-workers-remaining--id-"></code></pre>
</span>
<span id="execution-error-GETapi-workers-remaining--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-workers-remaining--id-"></code></pre>
</span>
<form id="form-GETapi-workers-remaining--id-" data-method="GET"
      data-path="api/workers/remaining/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-workers-remaining--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-workers-remaining--id-"
                    onclick="tryItOut('GETapi-workers-remaining--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-workers-remaining--id-"
                    onclick="cancelTryOut('GETapi-workers-remaining--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-workers-remaining--id-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/workers/remaining/{id}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="id"
               data-endpoint="GETapi-workers-remaining--id-"
               value="aut"
               data-component="url" hidden>
    <br>
<p>The ID of the remaining.</p>
            </p>
                    </form>

            <h2 id="endpoints-POSTapi-workers-newrequest">POST api/workers/newrequest</h2>

<p>
</p>



<span id="example-requests-POSTapi-workers-newrequest">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://test.test/api/workers/newrequest" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"email\": \"emery62@example.org\",
    \"vacation_start_date\": \"2102-05-06\",
    \"vacation_end_date\": \"2090-08-29\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/workers/newrequest"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "emery62@example.org",
    "vacation_start_date": "2102-05-06",
    "vacation_end_date": "2090-08-29"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-workers-newrequest">
</span>
<span id="execution-results-POSTapi-workers-newrequest" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-workers-newrequest"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-workers-newrequest"></code></pre>
</span>
<span id="execution-error-POSTapi-workers-newrequest" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-workers-newrequest"></code></pre>
</span>
<form id="form-POSTapi-workers-newrequest" data-method="POST"
      data-path="api/workers/newrequest"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-workers-newrequest', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-workers-newrequest"
                    onclick="tryItOut('POSTapi-workers-newrequest');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-workers-newrequest"
                    onclick="cancelTryOut('POSTapi-workers-newrequest');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-workers-newrequest" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/workers/newrequest</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="email"
               data-endpoint="POSTapi-workers-newrequest"
               value="emery62@example.org"
               data-component="body" hidden>
    <br>
<p>Must be a valid email address.</p>
        </p>
                <p>
            <b><code>vacation_start_date</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="vacation_start_date"
               data-endpoint="POSTapi-workers-newrequest"
               value="2102-05-06"
               data-component="body" hidden>
    <br>
<p>Must be a valid date. Must be a date after <code>yesterday</code>.</p>
        </p>
                <p>
            <b><code>vacation_end_date</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="vacation_end_date"
               data-endpoint="POSTapi-workers-newrequest"
               value="2090-08-29"
               data-component="body" hidden>
    <br>
<p>Must be a valid date. Must be a date after <code>vacation_start_date</code>.</p>
        </p>
        </form>

            <h2 id="endpoints-POSTapi-users-logout">User Logout user Login</h2>

<p>
</p>



<span id="example-requests-POSTapi-users-logout">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://test.test/api/users/logout" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/users/logout"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-users-logout">
</span>
<span id="execution-results-POSTapi-users-logout" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-users-logout"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-users-logout"></code></pre>
</span>
<span id="execution-error-POSTapi-users-logout" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-users-logout"></code></pre>
</span>
<form id="form-POSTapi-users-logout" data-method="POST"
      data-path="api/users/logout"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-users-logout', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-users-logout"
                    onclick="tryItOut('POSTapi-users-logout');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-users-logout"
                    onclick="cancelTryOut('POSTapi-users-logout');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-users-logout" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/users/logout</code></b>
        </p>
                    </form>

            <h2 id="endpoints-GETapi-users-loggeduser">Get User Logged in user with authorization enable</h2>

<p>
</p>



<span id="example-requests-GETapi-users-loggeduser">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://test.test/api/users/loggeduser" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/users/loggeduser"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-users-loggeduser">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;Unauthenticated.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-users-loggeduser" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-users-loggeduser"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-users-loggeduser"></code></pre>
</span>
<span id="execution-error-GETapi-users-loggeduser" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-users-loggeduser"></code></pre>
</span>
<form id="form-GETapi-users-loggeduser" data-method="GET"
      data-path="api/users/loggeduser"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-users-loggeduser', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-users-loggeduser"
                    onclick="tryItOut('GETapi-users-loggeduser');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-users-loggeduser"
                    onclick="cancelTryOut('GETapi-users-loggeduser');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-users-loggeduser" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/users/loggeduser</code></b>
        </p>
                    </form>

            <h2 id="endpoints-POSTapi-users-worker-create">Enable to create worker</h2>

<p>
</p>



<span id="example-requests-POSTapi-users-worker-create">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://test.test/api/users/worker/create" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"name\": \"cum\",
    \"email\": \"reilly.laurie@example.net\",
    \"password\": \"hson\",
    \"cpassword\": \"harum\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/users/worker/create"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "cum",
    "email": "reilly.laurie@example.net",
    "password": "hson",
    "cpassword": "harum"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-users-worker-create">
</span>
<span id="execution-results-POSTapi-users-worker-create" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-users-worker-create"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-users-worker-create"></code></pre>
</span>
<span id="execution-error-POSTapi-users-worker-create" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-users-worker-create"></code></pre>
</span>
<form id="form-POSTapi-users-worker-create" data-method="POST"
      data-path="api/users/worker/create"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-users-worker-create', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-users-worker-create"
                    onclick="tryItOut('POSTapi-users-worker-create');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-users-worker-create"
                    onclick="cancelTryOut('POSTapi-users-worker-create');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-users-worker-create" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/users/worker/create</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="name"
               data-endpoint="POSTapi-users-worker-create"
               value="cum"
               data-component="body" hidden>
    <br>

        </p>
                <p>
            <b><code>email</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="email"
               data-endpoint="POSTapi-users-worker-create"
               value="reilly.laurie@example.net"
               data-component="body" hidden>
    <br>
<p>Must be a valid email address.</p>
        </p>
                <p>
            <b><code>password</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="password"
               data-endpoint="POSTapi-users-worker-create"
               value="hson"
               data-component="body" hidden>
    <br>
<p>Must be at least 5 characters.</p>
        </p>
                <p>
            <b><code>cpassword</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="cpassword"
               data-endpoint="POSTapi-users-worker-create"
               value="harum"
               data-component="body" hidden>
    <br>
<p>The value and <code>password</code> must match.</p>
        </p>
        </form>

            <h2 id="endpoints-GETapi-users-requests-list">List of vacation request</h2>

<p>
</p>



<span id="example-requests-GETapi-users-requests-list">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://test.test/api/users/requests/list" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/users/requests/list"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-users-requests-list">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;Unauthenticated.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-users-requests-list" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-users-requests-list"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-users-requests-list"></code></pre>
</span>
<span id="execution-error-GETapi-users-requests-list" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-users-requests-list"></code></pre>
</span>
<form id="form-GETapi-users-requests-list" data-method="GET"
      data-path="api/users/requests/list"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-users-requests-list', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-users-requests-list"
                    onclick="tryItOut('GETapi-users-requests-list');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-users-requests-list"
                    onclick="cancelTryOut('GETapi-users-requests-list');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-users-requests-list" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/users/requests/list</code></b>
        </p>
                    </form>

            <h2 id="endpoints-POSTapi-users-update-request">Update Vacation Request Status</h2>

<p>
</p>



<span id="example-requests-POSTapi-users-update-request">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://test.test/api/users/update/request" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"worker_id\": 771339321.2661101,
    \"status\": \"rejected\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/users/update/request"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "worker_id": 771339321.2661101,
    "status": "rejected"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-users-update-request">
</span>
<span id="execution-results-POSTapi-users-update-request" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-users-update-request"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-users-update-request"></code></pre>
</span>
<span id="execution-error-POSTapi-users-update-request" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-users-update-request"></code></pre>
</span>
<form id="form-POSTapi-users-update-request" data-method="POST"
      data-path="api/users/update/request"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-users-update-request', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-users-update-request"
                    onclick="tryItOut('POSTapi-users-update-request');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-users-update-request"
                    onclick="cancelTryOut('POSTapi-users-update-request');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-users-update-request" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/users/update/request</code></b>
        </p>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <p>
            <b><code>worker_id</code></b>&nbsp;&nbsp;<small>number</small>  &nbsp;
                <input type="number"
               name="worker_id"
               data-endpoint="POSTapi-users-update-request"
               value="771339321.26611"
               data-component="body" hidden>
    <br>

        </p>
                <p>
            <b><code>status</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="status"
               data-endpoint="POSTapi-users-update-request"
               value="rejected"
               data-component="body" hidden>
    <br>
<p>Must be one of <code>approved</code> or <code>rejected</code>.</p>
        </p>
        </form>

            <h2 id="endpoints-GETapi-users-requests--id-">List of vacation request of individual workers</h2>

<p>
</p>



<span id="example-requests-GETapi-users-requests--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://test.test/api/users/requests/aliquam" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/users/requests/aliquam"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-users-requests--id-">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;Unauthenticated.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-users-requests--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-users-requests--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-users-requests--id-"></code></pre>
</span>
<span id="execution-error-GETapi-users-requests--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-users-requests--id-"></code></pre>
</span>
<form id="form-GETapi-users-requests--id-" data-method="GET"
      data-path="api/users/requests/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-users-requests--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-users-requests--id-"
                    onclick="tryItOut('GETapi-users-requests--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-users-requests--id-"
                    onclick="cancelTryOut('GETapi-users-requests--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-users-requests--id-" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/users/requests/{id}</code></b>
        </p>
                    <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <p>
                <b><code>id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
                <input type="text"
               name="id"
               data-endpoint="GETapi-users-requests--id-"
               value="aliquam"
               data-component="url" hidden>
    <br>
<p>The ID of the request.</p>
            </p>
                    </form>

            <h2 id="endpoints-GETapi-users-overrequest">Display the overlapping request</h2>

<p>
</p>



<span id="example-requests-GETapi-users-overrequest">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://test.test/api/users/overrequest" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://test.test/api/users/overrequest"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-users-overrequest">
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <details class="annotation">
            <summary>
                <small onclick="textContent = parentElement.parentElement.open ? 'Show headers' : 'Hide headers'">Show headers</small>
            </summary>
            <pre><code class="language-http">cache-control: no-cache, private
content-type: application/json
access-control-allow-origin: *
 </code></pre>
        </details>         <pre>

<code class="language-json">{
    &quot;message&quot;: &quot;Unauthenticated.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-users-overrequest" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-users-overrequest"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-users-overrequest"></code></pre>
</span>
<span id="execution-error-GETapi-users-overrequest" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-users-overrequest"></code></pre>
</span>
<form id="form-GETapi-users-overrequest" data-method="GET"
      data-path="api/users/overrequest"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}'
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-users-overrequest', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-users-overrequest"
                    onclick="tryItOut('GETapi-users-overrequest');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-users-overrequest"
                    onclick="cancelTryOut('GETapi-users-overrequest');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-users-overrequest" hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/users/overrequest</code></b>
        </p>
                    </form>

    

        
    </div>
    <div class="dark-box">
                    <div class="lang-selector">
                                                        <button type="button" class="lang-button" data-language-name="bash">bash</button>
                                                        <button type="button" class="lang-button" data-language-name="javascript">javascript</button>
                            </div>
            </div>
</div>
</body>
</html>
