<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\WorkerController;
use App\Http\Controllers\Api\VacationRequestController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
    /*****************
    *
    *  PUBLIC ROUTE
    * 
    ******************/ 
        
    /* -------- USER / MANAGER --------- */
    Route::post('/users/register',[UserController::class, 'register']);
    Route::post('/users/login',[UserController::class, 'login']);
    
    
    /* -------- Worker / Employee --------- */
    Route::post('/workers/login',[WorkerController::class, 'login']);
    Route::get('/workers', [WorkerController::class, 'index']);
    Route::get('/workers/{id}', [WorkerController::class, 'show']);
    
    
    /*****************
    *
    *  PROTECTED ROUTE
    * 
    ******************/ 
    Route::middleware(['auth:sanctum'])->group(function () {

        /* -------- Worker / Employee --------- */
        Route::post('/workers/logout',[WorkerController::class, 'logout']);

        /*************************
         *
         * TASKS WORKERS API ROUTE
         *  
         *************************/
        
        //See their requests
        Route::get('/workers/request/{id}', [WorkerController::class, 'getRequest']);
        // Remaining Holiday
        Route::get('/workers/remaining/{id}', [WorkerController::class, 'getRemainingHoliday']);
        // REQUEST NEW 
        Route::post('/workers/newrequest', [WorkerController::class, 'newRequest']);

        Route::put('/workers/profile/{id}', [WorkerController::class, 'update']);
        Route::put('/workers/credential/{id}', [WorkerController::class, 'update']);
        Route::delete('/workers/{id}', [WorkerController::class, 'destroy']);


        
        /* -------- USER / MANAGER --------- */
        Route::post('/users/logout',[UserController::class, 'logout']);
        Route::get('/users/loggeduser',[UserController::class, 'logged_user']);
        Route::post('/users/worker/create', [UserController::class, 'createWorker']);

        /*************************
         *
         * TASKS MANAGER API ROUTE
         *  
         *************************/

        //See an overview of all requests
        Route::get('/users/requests/list', [UserController::class, 'listRequest']);

        // Process an individual request and either approve or reject it
        Route::post('/users/update/request', [UserController::class, 'updateRequest']);
       
        //See an overview for each individual employee
        Route::get('/users/requests/{id}', [UserController::class, 'individualRequest']);
        
        //See an overview of overlapping requests
        Route::get('/users/overrequest', [UserController::class, 'overRequest']);
        
    });
