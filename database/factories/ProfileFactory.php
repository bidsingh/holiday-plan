<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Profile>
 */
class ProfileFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => User::pluck('id')->random(),
            'worker_id' => Worker::pluck('id')->random(),
            'joined_date' => now(),
            'date_birth' => '',
            'contact' => $this->faker->contact(),
            'street' => $this->faker->streetAddress(),
            'city' => $this->faker->city(),
            'postal_code' => $this->faker->postcode(),
            'position' => $this->faker->jobTitle(),
            'department' => '',
            'vacation_requested_count' => rand(1,30)
        ];
    }
}
