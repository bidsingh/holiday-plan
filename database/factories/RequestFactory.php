<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Worker;
use App\Models\User;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Request>
 */
class RequestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'worker_id' => Worker::pluck('id')->random(),
            'user_id' => 0,
            'status' => 'pending',
            'request_created_at' => now(),
            'vacation_start_date' => now(),
            'vacation_end_date' => now(),
            'vacation_taken_day' => rand(1,5),
        ];
    }
}
