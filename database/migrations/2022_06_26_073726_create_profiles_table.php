<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned()->default(0);
            $table->foreignId('worker_id')->references('id')->on('workers')->onDelete('cascade')->default(0);
            $table->dateTime('joined_date')->nullable();
            $table->date('date_birth')->nullable();
            $table->string('contact', 15)->nullable();
            $table->string('street', 45)->nullable();
            $table->string('city', 45)->nullable();
            $table->string('postal_code', 45)->nullable();
            $table->integer('vacation_requested_count')->unsigned()->default(0);
            $table->integer('vacation_taken_count')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
};
