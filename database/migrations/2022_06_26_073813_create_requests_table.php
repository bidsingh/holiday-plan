<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('worker_id')->references('id')->on('workers')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->default(0);
            $table->enum('status', ['approved', 'pending', 'rejected'])->default('pending');
            $table->timestamp('request_created_at')->nullable();
            $table->timestamp('vacation_start_date')->nullable();
            $table->timestamp('vacation_end_date')->nullable();
            $table->integer('vacation_taken_day')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
};
