<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VacationRequest extends Model
{
    use HasFactory;

    protected $table = 'requests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'worker_id',
        'request_created_at',
        'vacation_start_date',
        'vacation_end_date',
        'vacation_taken_day',
        'street',
        'city',
        'postal_code',
        'position',
        'department',
        'vacation_requested_count'
    ];
}
