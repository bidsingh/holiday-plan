<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'worker_id',
        'image',
        'joined_date',
        'date_birth',
        'contact',
        'street',
        'city',
        'postal_code',
        'position',
        'department',
        'vacation_requested_count'
    ];

}
