<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class VacationStatusRequest extends FormRequest
{
    /**
     * Overwrite protected method "failedValidation" method
     *
     * @return array<string, mixed>
     */ 
    
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'Validation Errors',
            'data'    => $validator->errors(),
        ], 422));
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $this->check = ['approved', 'rejected'];
        return [
            'worker_id'=> ['required', 'numeric'],
            'status' => [
                'required',
                Rule::in($this->check)
            ]
        ];
    }
}
