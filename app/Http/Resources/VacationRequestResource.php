<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VacationRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "author" => $this->worker_id,
            "status" => $this->status,
            "resolved_by" => ($this->user_id == 0) ? 'not yet' : $this->user_id,
            "request_created_at" => $this->request_created_at,
            "vacation_start_date" => $this->vacation_start_date,
            "vacation_end_date" => $this->vacation_end_date,
        ];
    }
}
