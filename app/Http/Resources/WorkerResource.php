<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WorkerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
          'id' => $this->worker_id,
          'name' => $this->name,  
          'email' => $this->email,  
          'joined_date' => $this->joined_date,
          'date_of_birth' => $this->date_birth,
          'contact' => $this->contact,
          'street' => $this->street,
          'city' => $this->city,
          'postal_code' => $this->postal_code,
          'position' => $this->position,
          'department' => $this->department,
          'created_at' => $this->created_at,
          'updated_at'=> $this->updated_at,
        ];
    }
}
