<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OverRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'author' => $this->worker_id,  
            'request_threshold' => 30,
            'over_request_status' => true,
            'request_remaining' => 30 - $this->vacation_requested_count,
            'created_at' => $this->created_at,
            'updated_at'=> $this->updated_at,
          ];
    }
}
