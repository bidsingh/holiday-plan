<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Http\Requests\Vacation\StoreRequest;
use App\Http\Resources\VacationRequestResource;
use App\Models\VacationRequest;
use App\Models\Setting;
use App\Models\Worker;
use App\Models\Profile;

class VacationRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*
         * Without Filter status query string
         * return all list of request
         * 
         */ 
        $vacation = VacationRequest::all();
        $message  = "Requested Vacation List";

        /*
         * Adding Filter with status query string
         * api/vacation/requested/list?status=  STATUS_OPTION [default = 'all', 'pending', 'approved', 'rejected']
         * 
         */ 
        if ($request->status AND !isset($request->worker_id)) {

            // DEFAULT STATUS DATA
            $vacation = VacationRequest::all();
            $message  = ucfirst($request->status)." Status Data List";
            
            if ($request->status !== 'all') {

                $vacation = VacationRequest::where('status', $request->status)->get();

            } 

            if (!sizeof($vacation)) {

                return response()->json([
                    "status" => false, 
                    'message' =>  $request->status." status request data not found",], 
                    Response::HTTP_NOT_FOUND);

            }

        }


        /*
         * See an overview for each individual employee
         * Adding Worker ID query string
         * api/vacation/requested/list?worker_id=  OPTION [specific id]
         * 
         */ 
        if ($request->worker_id AND !isset($request->status)) {

            $vacation = VacationRequest::where('worker_id', $request->worker_id)->get();
            $message  = "Worker ID ".$request->worker_id." Requested Data List";

            if (!sizeof($vacation)) {

                return response()->json([
                    "status" => false, 
                    'message' =>  $request->worker_id." worker ID request data not found",], 
                    Response::HTTP_NOT_FOUND);

            }

        }

        /*
         * Process an individual request and either approve or reject it
         * 
         * api/vacation/requested/list?worker_id=  OPTION [specific id] &status = STATUS_OPTION ['all', 'approved', 'rejected']
         * 
         */ 
        if (isset($request->worker_id) AND isset($request->status)) {

            $vacation = VacationRequest::select('*')
                                        ->where('worker_id', '=', $request->worker_id)
                                        ->where('status', '=' , $request->status)
                                        ->get();

            if ($request->status == 'all') {

                $vacation = VacationRequest::where('worker_id', $request->worker_id)->get();

            }

            $message  = "Worker ID ".$request->worker_id." with status ".$request->status." Requested Data List";

            if (!sizeof($vacation)) {

                return response()->json([
                    "status" => false, 
                    'message' =>  $request->worker_id." worker ID with status ".$request->status." request data not found",], 
                    Response::HTTP_NOT_FOUND);

            }

        }
            
        return response()->json([
            "status"=> true, 
            'message'=> $message, 
            "data" => VacationRequestResource::collection($vacation)] , Response::HTTP_OK);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $validated = $request->validated();

        // Verifying via email
        $worker = Worker::where('email', $request->email)->first();

        if ($worker) {

            // Get Worker Profile Detail with only worker_id and vacation_requested_count
            $workerProfileDetail = Profile::select('worker_id', 'vacation_requested_count')
                                            ->where('worker_id', $worker->id)
                                            ->first();

            /*
            * Verify Request Threshold as setting used by worker
            * 
            */
            $setting = Setting::first();

            if (($workerProfileDetail->vacation_requested_count == $setting->total_holiday_request_threshold) ||
                ($workerProfileDetail->vacation_requested_count > $setting->total_holiday_request_threshold)) {

                return response()->json([
                'success'  => false,
                'message' => 'Cannot make request! Request Limit Exceeded',
                ], Response::HTTP_TOO_MANY_REQUESTS);


            } else {

                /*
                * Check if same worker requesting vacation after sending request that is in pending status
                * if worker request is in pending status then further without status 'approved' or 'rejected' then worker
                * cannot request till that situation
                * 
                */

                $checkSameWorkerRequest = VacationRequest::select('status')->where('worker_id', $worker->id)->first();

                if (isset($checkSameWorkerRequest)) {

                    if ($checkSameWorkerRequest->status == 'pending') {

                        return response()->json([
                            'success'  => false,
                            'message' => 'Vacation Request has been already requested ! Please wait until decision made by Manager',
                            ], Response::HTTP_FORBIDDEN);

                    }

                }  

                    /*
                    *
                    * Insert Vacation Requested Data in VacationRequest
                    * 
                    */
                    $vacationRequestField = new VacationRequest();

                    $vacationRequestField->worker_id = $worker->id;
                    $vacationRequestField->status = 'pending';
                    $vacationRequestField->request_created_at = date('Y-m-d H:i:s');
                    $vacationRequestField->vacation_start_date = $request->vacation_start_date;
                    $vacationRequestField->vacation_end_date = $request->vacation_end_date;

                    // Get Day by Two Date
                    $start_date = strtotime($vacationRequestField->vacation_start_date);
                    $end_date = strtotime($vacationRequestField->vacation_end_date);

                    // 86400 is the timestamp difference of a day
                    $vacationRequestField->vacation_taken_day = (int)(($end_date - $start_date)/86400);

                    $vacationRequestField->save();

                    /*
                    *
                    * Update Profile Vacation Requested Count Column Adding Days Holiday Requested
                    * 
                    */
                    // $updateProfileData = [
                    //     'vacation_requested_count' => $workerProfileDetail + $vacationRequestField->vacation_taken_day
                    // ];

                    // Profile::where('id', $worker->id)->update($updateProfileData);

                    

                    return response()->json([
                        'success'  => true,
                        'message' => 'Successfully Vacation Requested',
                        'data' => new VacationRequestResource($vacationRequestField)
                        ], Response::HTTP_CREATED);

            }

        }
        
    }

}
