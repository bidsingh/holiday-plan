<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests\Worker\StorePostRequest;
use App\Http\Requests\Vacation\StoreRequest;
use App\Http\Requests\User\UserLoginRequest;
use App\Http\Resources\WorkerResource;
use App\Http\Resources\VacationRequestResource;
use App\Http\Resources\OverRequestResource;
use App\Models\Setting;
use App\Models\Worker;
use App\Models\Profile;
use App\Models\VacationRequest;

/**
 * @group Worker API Request
 *
 * 
 */
class WorkerController extends Controller
{
    /**
     * Workers List
     *
     * Listing all workers with detail information
     * 
     * @response {
     *  "success": "true",
     *  "Message" : "List of Workers",
     *  "data": [ 
     *      "id" : "1", 
     *      "name" : "Name",
     *      "email" : "example@email.com",
     *      "joined_date": "2022-06-28 06:09:32",
	 *		"date_of_birth": "",
	 *		"contact": "",
     *	    "street": "",
	 *		"city": "",
	 *		"postal_code": "",
	 *		"position": "",
	 *		"department": "",
	 *		"created_at": "2022-06-28T06:09:32.000000Z",
	 *		"updated_at": "2022-06-29T09:59:38.000000Z"
     *   ]
     * }
     */
    public function index()
    {
        // retrieve all data by joining worker and profile
        $all = Worker::join('profiles', 'workers.id', '=', 'profiles.worker_id')
                        ->get(['workers.*', 'profiles.*']);
        
        return response()->json([
            'success'  => true,
            'message' => 'List of Workers',
            'data' => WorkerResource::collection($all),
            ], Response::HTTP_OK);

    }
    

    /**
     * Individual Worker Details.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @response {
     *  "success": "true",
     *  "Message" : "Individual Worker Detail",
     *  "data": [ 
     *      "id" : "1", 
     *      "name" : "Name",
     *      "email" : "example@email.com",
     *      "joined_date": "2022-06-28 06:09:32",
	 *		"date_of_birth": "",
	 *		"contact": "",
     *	    "street": "",
	 *		"city": "",
	 *		"postal_code": "",
	 *		"position": "",
	 *		"department": "",
	 *		"created_at": "2022-06-28T06:09:32.000000Z",
	 *		"updated_at": "2022-06-29T09:59:38.000000Z"
     *   ]
     * }
     */
    public function show($id)
    {
        /*
         *  Displaying specific individual
         * 
         * 
         */ 
        $all = Worker::join('profiles', 'workers.id', '=', 'profiles.worker_id')
                        ->where('workers.id', $id)
                        ->get(['workers.*', 'profiles.*'])->first();

        return response()->json([
            'success'  => true,
            'message' => 'Individual Worker Detail',
            'data' => new WorkerResource($all),
            ], Response::HTTP_OK);
    }

    

    /**
     * Updating Worker Details
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *  "success": "true",
     *  "Message" : "Profile Data Updated",
     * }
     *
     */
    public function update(Request $request, $id)
    {
        $workerData = Worker::findorfail($id);

        // Get URL segment
        $segment = \Request::segment(3);

        if ($segment == 'profile') {

            /* 
             *  Updating Profile associated columns
             * 
             */

            $profileDataHavingWorkerData = Profile::where('worker_id', $workerData->id)->get();

            $updatingProfileData = [
                "date_birth" => ($request->has('date_birth')) ? $request->date_birth : NULL,
                "contact" => ($request->has('contact')) ? $request->contact : '',
                "street" => ($request->has('street')) ? $request->street : '',
                "city" => ($request->has('city')) ? $request->city : '',
                "postal_code" => ($request->has('postal_code')) ? $request->postal_code : '',
                "position" => ($request->has('position')) ? $request->position : '',
                "department" => ($request->has('department')) ? $request->department : '',
            ];

            Profile::where('worker_id', $workerData->id)->update($updatingProfileData);

            $message = 'Profile Data Updated';

        } else {

            /* 
             *  Updating Worker associated columns
             * 
             */

            $request->validate([
                'name' => 'required',
                'password'=> 'required|min:5',
                'oldpassword'=>'required',
            ]);

            if(!Hash::check($request->oldpassword, $workerData->password)) {

	    		return response()->json([
                    'success'  => false,
                    'message' => 'Old Password Does not Match',
                    ], Response::HTTP_FORBIDDEN);

	    	}

            $updatingCredentialData = [
                "name" => $request->name,
                "password" => Hash::make($request->password),
            ];

            Worker::where('id', $workerData->id)->update($updatingCredentialData);

            $message = 'Credential Data Updated';

        } 

        return response()->json([
            'success'  => true,
            'message' => $message,
            ], Response::HTTP_CREATED);

    }

    /**
     * Remove the Individual.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Worker::where('id',$id)->delete();
        return response()->json([
            'success'  => true,
            'message' => 'Data Deleted',
            ], Response::HTTP_NO_CONTENT);
    }

    /**
     * Registered user Login 
     *
     * @param  App\Http\Requests\User\UserLoginRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function login(UserLoginRequest $request)
    {   
        $request->validated();

        $worker = Worker::where('email', $request->email)->first();

        if ($worker && Hash::check($request->password, $worker->password)) {
            $token = $worker->createToken($request->email)->plainTextToken;
            return response()->json([
                'success'  => true,
                'message' => 'Login Successfully',
                'data' => $worker,
                'token' => $token
                 ] , Response::HTTP_OK);
        } else {
            return response()->json([
                'success'  => false,
                'message' => 'Provided Credential does not match',
                 ] , Response::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * Worker Logout user Login 
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {   
        auth()->user()->tokens()->delete();

        return response()->json([
            'success'  => true,
            'message' => 'logout Successfully',
             ] , Response::HTTP_OK);
    }


    /**
     * Display the request holiday of individual
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getRequest(Request $request, $id)
    {
        if ($request->status) {

            $vacation = VacationRequest::select('*')
                                        ->where('worker_id', '=', $id)
                                        ->where('status', '=' , $request->status)
                                        ->get();

            if ($request->status == 'all') {

                $vacation = VacationRequest::where('worker_id', $id)->get();

            }

            if (!sizeof($vacation)) {

                return response()->json([
                    "status" => false, 
                    'message' =>  $id." worker ID with status ".$request->status." request data not found"], Response::HTTP_NOT_FOUND);

            }

            return response()->json([
                "status"=> true, 
                'message'=> $id." Worker ID Requested Data with ".$request->status." status", 
                "data" => VacationRequestResource::collection($vacation)] , Response::HTTP_OK);

        } else {

            $vacation = VacationRequest::where('worker_id', $id)->get();

            return response()->json([
                "status"=> true, 
                'message'=> $id." Worker ID Requested Data", 
                "data" => VacationRequestResource::collection($vacation)] , Response::HTTP_OK);

        }
    
        
        
    }

    

    /**
     * Display the remaining holiday of individual
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *  "success": "true",
     *  "Message" : "Remaining Holiday Days",
     *  "data": [ 
     *      "id" : "1", 
     *      "author" : "worker_id",
     *      "email" : "example@email.com",
     *      "remaining_holiday_days": "",
	 *		"created_at": "2022-06-28T06:09:32.000000Z",
	 *		"updated_at": "2022-06-29T09:59:38.000000Z"
     *   ]
     * }
     */
    public function getRemainingHoliday($id) 
    {
        $data = Profile::where('worker_id', $id)->first();

        // FETCH SETTING DATA ROW
        $settingData = Setting::first();

        if ($data) {

            return response()->json([
                'success'  => true,
                'message' => 'Remaining Holiday Days',
                'data' => [
                    'id' => $data->id,
                    'author' => $data->worker_id,
                    'remaining_holiday_days' => $settingData->total_holiday_allowed - $data->vacation_requested_count,
                    'created_at' => $data->created_at,
                    'updated_at' => $data->updated_at
                ]
                ], Response::HTTP_OK);

        } else {

            return response()->json([
                'success'  => false,
                'message' => 'Worker ID data not found',
                ], Response::HTTP_NOT_FOUND);

        }

        
    }

    /**
     * New Request from worker
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *  "success": "true",
     *  "Message" : "Remaining Holiday Days",
     *  "data": [ 
     *      "id" : "1", 
     *      "author" : "worker_id",
     *      "status" : "pending",
     *      "resolved_by" : "not yet",
     *      "request_created_at": "",
	 *		"vacation_start_date": "2022-06-28T06:09:32.000000Z",
	 *		"vacation_end_date": "2022-06-29T09:59:38.000000Z"
     *   ]
     * }
     */

    public function newRequest(StoreRequest $request)
    {
        $validated = $request->validated();

        // Verifying via email
        $worker = Worker::where('email', $request->email)->first();

        
        if ($worker) {
            
            // Get Worker Profile Detail with only worker_id and vacation_requested_count
            $workerProfileDetail = Profile::select('worker_id', 'vacation_requested_count', 'vacation_taken_count')
                                            ->where('worker_id', $worker->id)
                                            ->first();

            /*
            * Verify Request Threshold as setting used by worker
            * 
            */
            $setting = Setting::first();

            if ($workerProfileDetail->vacation_requested_count == $setting->total_holiday_request_threshold) {
                return response()->json([
                    'success'  => false,
                    'message' => 'Cannot make request! Request Limit Exceeded',
                    ], Response::HTTP_TOO_MANY_REQUESTS);
            
            } elseif ($workerProfileDetail->vacation_taken_count == $setting->total_holiday_allowed) {

                return response()->json([
                    'success'  => false,
                    'message' => 'Cannot make request! Holiday Quote Finished',
                    ], Response::HTTP_TOO_MANY_REQUESTS);


            } else {



                /*
                * Check if same worker requesting vacation after sending request that is in pending status
                * if worker request is in pending status then further without status 'approved' or 'rejected' then worker
                * cannot request till that situation
                * 
                */

                $checkSameWorkerRequest = VacationRequest::select('status')->where('worker_id', $worker->id)->orderBy('id', 'desc')->first();

                
                if (isset($checkSameWorkerRequest)) {

                    if ($checkSameWorkerRequest->status == 'pending') {

                        return response()->json([
                            'success'  => false,
                            'message' => 'Vacation Request has been already requested ! Please wait until decision made by Manager',
                            ], Response::HTTP_FORBIDDEN);

                    }

                }  

                    /*
                    *
                    * Insert Vacation Requested Data in VacationRequest
                    * 
                    */
                    $vacationRequestField = new VacationRequest();

                    $vacationRequestField->worker_id = $worker->id;
                    $vacationRequestField->status = 'pending';
                    $vacationRequestField->request_created_at = date('Y-m-d H:i:s');
                    $vacationRequestField->vacation_start_date = $request->vacation_start_date;
                    $vacationRequestField->vacation_end_date = $request->vacation_end_date;

                    // Get Day by Two Date
                    $start_date = strtotime($vacationRequestField->vacation_start_date);
                    $end_date = strtotime($vacationRequestField->vacation_end_date);

                    // 86400 is the timestamp difference of a day
                    $vacationRequestField->vacation_taken_day = (int)(($end_date - $start_date)/86400);

                    $vacationRequestField->save();

                    return response()->json([
                        'success'  => true,
                        'message' => 'Successfully Vacation Requested',
                        'data' => new VacationRequestResource($vacationRequestField)
                        ], Response::HTTP_CREATED);

            }

        }
    }
}
