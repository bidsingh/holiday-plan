<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Worker\StorePostRequest;
use App\Http\Requests\User\UserLoginRequest;
use App\Http\Requests\User\VacationStatusRequest;
use App\Http\Resources\VacationRequestResource;
use App\Http\Resources\OverRequestResource;
use App\Http\Resources\WorkerResource;
use App\Models\User;
use App\Models\Profile;
use App\Models\Worker;
use App\Models\VacationRequest;

/**
 * @group Manager API Section
 *
 * API Token Request to call API after logining in
 */

class UserController extends Controller
{
    /**
     * Register New Manager.
     * 
     * API Token not required
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *  "success": "true",
     *  "Message" : "Successfully Register",
     *  "data": [ 
     *      "id" : "1", 
     *      "name" : "Name",
     *      "email" : "example@email.com",
	 *		"created_at": "2022-06-28T06:09:32.000000Z",
	 *		"updated_at": "2022-06-29T09:59:38.000000Z"
     *   ],
     *  "token": "{TOKEN_generated}"
     * }
     */
    public function register(StorePostRequest $request)
    {
        $request->validated();

        if (User::where('email', $request->email)->first()) {
            return response([
                'status' => false,
                'message' => 'Email Already Exists'], Response::HTTP_OK);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' =>  Hash::make($request->password)
        ]);

        $token = $user->createToken($request->email)->plainTextToken;

        return response()->json([
            'success'  => true,
            'message' => 'Successfully Register',
            'data' => $user,
            'token' => $token
             ] , Response::HTTP_CREATED);
    }


    /**
     * Registered Manager Login 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     *  @response {
     *  "success": "true",
     *  "Message" : "Login Successfully",
     *  "data": [ 
     *      "id" : "1", 
     *      "name" : "Name",
     *      "email" : "example@email.com",
	 *		"created_at": "2022-06-28T06:09:32.000000Z",
	 *		"updated_at": "2022-06-29T09:59:38.000000Z"
     *   ],
     *  "token": "{TOKEN_generated}"
     * }
     */
    public function login(UserLoginRequest $request)
    {   
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = User::where('email', $request->email)->first();

        if ($user && Hash::check($request->password, $user->password)) {
            $token = $user->createToken($request->email)->plainTextToken;
            return response()->json([
                'success'  => true,
                'message' => 'Login Successfully',
                'data' => $user,
                'token' => $token
                 ] , Response::HTTP_OK);
        } else {
            return response()->json([
                'success'  => false,
                'message' => 'Provided Credential does not match',
                 ] , Response::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * Manager Logout
     *
     * Bearer Token Required to form API
     * 
     * @return \Illuminate\Http\Response
    *  @response {
     *  "success": "true",
     *  "Message" : "Logou Successfully",
     *  
     * }
     */
    public function logout()
    { 
        auth()->user()->tokens()->delete();

        return response()->json([
            'success'  => true,
            'message' => 'logout Successfully',
             ] , Response::HTTP_OK);
    }

    /**
     * Get User Logged in user with authorization enable
     *
     * Bearer Token Required to form API
     * 
     * @return \Illuminate\Http\Response
     * 
     * * @response {
     *  "success": "true",
     *  "Message" : "Logged Use",
     *  "data": [ 
     *      "user" : "current_user_login", 
     *      
     *   ]
     * }
     */
    public function logged_user() 
    {
        $loggedUser = auth()->user();
        return response()->json([
            'status' => true,
            'message' => 'Logged User',
            'data' => ['user' => $loggedUser]
        ], Response::HTTP_OK);
    }

    /**
     * Manager Allowed to create Worker Account
     *
     * @param  App\Http\Requests\Worker\StorePostRequest  $request
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *  "success": "true",
     *  "Message" : "New Worker Data Created",
     *  "data": [ 
     *      "id" : "1", 
     *      "name" : "Name",
     *      "email" : "example@email.com",
     *      "joined_date": "2022-06-28 06:09:32",
	 *		"date_of_birth": "",
	 *		"contact": "",
     *	    "street": "",
	 *		"city": "",
	 *		"postal_code": "",
	 *		"position": "",
	 *		"department": "",
	 *		"created_at": "2022-06-28T06:09:32.000000Z",
	 *		"updated_at": "2022-06-29T09:59:38.000000Z"
     *   ]
     * }
     */
    public function createWorker(StorePostRequest $request)
    {
        $request->validated();

        $workerField = new Worker();

        // POPULATING WORKER COLUMN WITH REQUEST DATA
        $workerField->name = $request->name;
        $workerField->email = $request->email;
        $workerField->password = Hash::make($request->password);
        $workerField->save();

        $profileField = new Profile();
        
        // POPULATING PROFILE COLUMN WITH WORKER FOREIGN KEY
        $profileField->user_id = 0;
        $profileField->worker_id = $workerField->id;
        $profileField->joined_date = date('Y-m-d H:i:s');
        $profileField->date_birth = ($request->has('date_birth')) ? $request->date_birth : NULL;
        $profileField->contact = ($request->has('contact')) ? $request->contact : '';
        $profileField->street = ($request->has('street')) ? $request->street : '';
        $profileField->city = ($request->has('city')) ? $request->city : '';
        $profileField->postal_code = ($request->has('postal_code')) ? $request->postal_code : '';
        $profileField->position = ($request->has('position')) ? $request->position : '';
        $profileField->department = ($request->has('department')) ? $request->department : '';
        $profileField->vacation_requested_count = 0;

        $profileField->save();

        // retrieve specific data by joining worker and profile
        $worker = Worker::join('profiles', 'workers.id', '=', 'profiles.worker_id')
                        ->where('workers.id', $workerField->id)
                        ->get(['workers.*', 'profiles.*'])
                        ->first();

        $token = $worker->createToken($request->email)->plainTextToken;

        if ($worker) {
            return response()->json([
                'success'  => true,
                'message' => 'New Worker Data Created',
                'data' => new WorkerResource($worker),
                'token'=> $token,
                ], Response::HTTP_CREATED);

        } else {

            return response()->json([
                'success'  => false,
                'message' => 'Unable to create data',
                 ] , Response::HTTP_BAD_REQUEST);

        }
    }

     /**
     * Process an individual request and either approve or reject it
     * 
     * Bearer Token Required
     *
     * @param  App\Http\Requests\User\VacationStatusRequest $request
     * @return \Illuminate\Http\Response
     * 
     *  @response {
     *  "success": "true",
     *  "Message" : "Vacation Request Status Updated",
     *  "data": [ 
     *      "id" : "1", 
     *      "author" : "worker_id",
     *      "status" : ["approved", "rejected"],
     *      "resolved_by": "manager_id",
	 *		"request_created_at": "request_date",
	 *		"vacation_start_date": "selected start date",
     *	    "vacation_end_date": "selected end date",
     *   ]
     * }
     */
    public function updateRequest(VacationStatusRequest $request) 
    {
        $request->validated();

        // check worker_id available with pendong status
        $workerData = VacationRequest::select('*')
                                    ->where('worker_id', '=', $request->worker_id)
                                    ->where('status', '=', 'pending')
                                    ->first();

        if (!$workerData) {
            return response()->json([
                'status' => false,
                'message' => 'No Worker ID with pending status found out',
            ], Response::HTTP_NOT_FOUND);
        }
        
        // Get user data
        $loggedUser = auth()->user();

        // return $loggedUser->id;

        // ON APPROVED ASSIGN HOLIDAY GRANT OR REJECTED ZERO
        $holiday_count = ($request->status == 'approved') ? $workerData->vacation_taken_day : 0;
        
        // get worker profile data 
        $getWorkerProfileData = Profile::select('vacation_requested_count','vacation_taken_count')->where('worker_id', $request->worker_id)->first();

        $updateData = [
            "vacation_requested_count" => $getWorkerProfileData->vacation_requested_count + 1,
            "vacation_taken_count"    => $getWorkerProfileData->vacation_taken_count + $holiday_count
        ];
        
        // Update Profile Table
        Profile::where('worker_id', $request->worker_id)->update($updateData);

        // Update Request Tablte
        $updatedData = VacationRequest::where('id', $workerData->id)->update(['status'=>$request->status, 'user_id' => $loggedUser->id]);

        $publishData = VacationRequest::where('worker_id', $request->worker_id)->first();
        
        return response()->json([
            'status' => true,
            'message' => 'Vacation Request Status Updated',
            'data' => new VacationRequestResource($publishData)
        ], Response::HTTP_CREATED);
    }


    /**
     * List of Vacation Request By Workers
     *
     * Bearer Token Required 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * @response {
     *  "success": "true",
     *  "Message" : "Vacation Request",
     *  "data": [ 
     *      "id" : "1", 
     *      "author" : "worker_id",
     *      "status" : ["approved", "rejected"],
     *      "resolved_by": "manager_id",
	 *		"request_created_at": "request_date",
	 *		"vacation_start_date": "selected start date",
     *	    "vacation_end_date": "selected end date",
     *   ]
     * }
     */
    public function listRequest(Request $request)
    {
        /*
         * Without Filter status query string
         * return all list of request
         * 
         */ 
        $vacation = VacationRequest::all();
        $message  = "Requested Vacation List";

        /*
         * Adding Filter with status query string
         * api/vacation/requested/list?status=  STATUS_OPTION [default = 'all', 'pending', 'approved', 'rejected']
         * 
         */ 
        if ($request->status AND !isset($request->worker_id)) {

            // DEFAULT STATUS DATA
            $vacation = VacationRequest::all();
            $message  = ucfirst($request->status)." Status Data List";
            
            if ($request->status !== 'all') {

                $vacation = VacationRequest::where('status', $request->status)->get();

            } 

            if (!sizeof($vacation)) {

                return response()->json([
                    "status" => false, 
                    'message' =>  $request->status." status request data not found",], 
                    Response::HTTP_NOT_FOUND);

            }

        }


        /*
         * See an overview for each individual employee
         * Adding Worker ID query string
         * api/vacation/requested/list?worker_id=  OPTION [specific id]
         * 
         */ 
        if ($request->worker_id AND !isset($request->status)) {

            $vacation = VacationRequest::where('worker_id', $request->worker_id)->get();
            $message  = "Worker ID ".$request->worker_id." Requested Data List";

            if (!sizeof($vacation)) {

                return response()->json([
                    "status" => false, 
                    'message' =>  $request->worker_id." worker ID request data not found",], 
                    Response::HTTP_NOT_FOUND);

            }

        }

        /*
         * Process an individual request and either approve or reject it
         * 
         * api/vacation/requested/list?worker_id=  OPTION [specific id] &status = STATUS_OPTION ['all', 'approved', 'rejected']
         * 
         */ 
        if (isset($request->worker_id) AND isset($request->status)) {

            $vacation = VacationRequest::select('*')
                                        ->where('worker_id', '=', $request->worker_id)
                                        ->where('status', '=' , $request->status)
                                        ->get();

            if ($request->status == 'all') {

                $vacation = VacationRequest::where('worker_id', $request->worker_id)->get();

            }

            $message  = "Worker ID ".$request->worker_id." with status ".$request->status." Requested Data List";

            if (!sizeof($vacation)) {

                return response()->json([
                    "status" => false, 
                    'message' =>  $request->worker_id." worker ID with status ".$request->status." request data not found",], 
                    Response::HTTP_NOT_FOUND);

            }

        }
            
        return response()->json([
            "status"=> true, 
            'message'=> $message, 
            "data" => VacationRequestResource::collection($vacation)] , Response::HTTP_OK);

    }

    /**
     * List of vacation request of individual workers
     *
     * Bearer Token Required
     * 
     * @param  int $id
     * @return \Illuminate\Http\Response
     * 
     *  @response {
     *  "success": "true",
     *  "Message" : "List of vacation request of individual workers",
     *  "data": [ 
     *      "id" : "1", 
     *      "author" : "worker_id",
     *      "status" : ["approved", "rejected"],
     *      "resolved_by": "manager_id",
	 *		"request_created_at": "request_date",
	 *		"vacation_start_date": "selected start date",
     *	    "vacation_end_date": "selected end date",
     *   ]
     * }
     */
    public function individualRequest($id)
    {
        $vacation = VacationRequest::where('worker_id', $id)->get();

        if (!sizeof($vacation)) {
            return response()->json([
                "status"=> false, 
                'message'=> "Worker ID ".$id." Requested List Not Found", 
                "data" => []] , Response::HTTP_NOT_FOUND);
        }

        return response()->json([
            "status"=> true, 
            'message'=> "Worker ID ".$id." Requested List", 
            "data" => VacationRequestResource::collection($vacation)] , Response::HTTP_OK);

    }

    /**
     * Displaying the overlapping request
     *
     * Bearer Token Required
     * 
     * @return \Illuminate\Http\Response
     * 
     *  @response {
     *  "success": "true",
     *  "Message" : "List of Overlapping Requests",
     *  "data": [ 
     *      "id" : "1", 
     *      "author" : "worker_id",
     *      "request_threshold" : "30",
     *      "over_request_status": "truw",
	 *		"request_remaining": "0",
	 *		"created_at": "created date",
     *	    "updated_at": "update date",
     *   ]
     * } 
     */
    public function overRequest()
    {
        $overRequestData = Profile::where('vacation_requested_count', 30)->get();

        $message = "List of Overlapping Requests";
        $response = Response::HTTP_OK;
        $data = OverRequestResource::collection($overRequestData);

        if (!sizeof($overRequestData)) {

            $message = "No Overlapping Request Data list Found";
            $response = Response::HTTP_NOT_FOUND;
            $data = [];

        }

        return response()->json([
            'success'  => true,
            'message' => $message,
            'data' => $data,
            ], $response);
       
    }


}
