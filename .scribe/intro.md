# Introduction



This API documentation of Holiday Plan Tasks.

<aside>
<h2>Tasks</h2>
<ol>
    <li>There should be API routes that allow workers to:
        <ul>
            <li>See their requests
                <ul>
                    <li>Filter by status (approved, pending, rejected)</li>
                </ul>
            </li>
            <li>See their number of remaining vacation days</li>
        <li>Make a new request if they have not exhausted their total limit (30 per year)</li>
        </ul>
    </li>

    <li>There should be API routes that allow managers to:
        <ul>
            <li>See an overview of all requests
                <ul>
                    <li>Filter by pending and approved</li>
                </ul>
            </li>
            <li>See an overview for each individual employee</li>
            <li>See an overview of overlapping requests</li>
            <li>Process an individual request and either approve or reject it</li>
        </ul>
    </li>
    
</ol>
</aside>

> Base URL

```yaml
http://test.test
```